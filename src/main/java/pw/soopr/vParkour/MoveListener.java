package pw.soopr.vParkour;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
public class MoveListener implements Listener{

    HashMap<String, Parkour> parkouring = new HashMap<String, Parkour>();

    Main plugin;
    public MoveListener (Main main){
        this.plugin = main;
    }

    @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMove(PlayerMoveEvent e){
        Player p = e.getPlayer();

        for(Parkour parkour : plugin.getParkourList()){
            if(p.getWorld().equals(parkour.getWorld())){
                Location loc = parkour.getFirstBlock().add(0,1,0).getBlock().getLocation().clone();
                if(e.getTo().getBlock().getLocation().equals(loc) || p.getLocation().getBlock().getLocation().equals(loc)){
                    if(!parkouring.containsKey(p.getName())) {
                        parkouring.put(p.getName(), parkour);
                        p.sendMessage(ChatColor.AQUA + "Parkour has begun.");
                    }
                  }

            }
        }
        if(parkouring.isEmpty()) return;
        if(parkouring.containsKey(e.getPlayer().getName())){
            Parkour parkour = parkouring.get(p.getName());
            if(e.getTo().getBlock().getLocation().equals(parkour.getFinish().add(0,1,0).getBlock().getLocation()) || p.getLocation().getBlock().getLocation().equals(parkour.getFinish().add(0,1,0).getBlock().getLocation())){
                Location spawn = parkour.getSpawn();
                spawn.setPitch(plugin.getParkourConfig().getInt(parkour.getName() + ".spawn.pitch"));
                spawn.setYaw(plugin.getParkourConfig().getInt(parkour.getName() + ".spawn.yaw"));
                p.sendMessage(ChatColor.AQUA + "Congratulations! You beat the parkour.");
                        if(Cooldown.isCooling(p.getName(), parkour.getName())){
                            p.sendMessage(ChatColor.AQUA + "You have already completed this parkour today! Try again in 24 hours for a prize.");
                            parkouring.remove(p.getName());
                            return;
                        }


                try{
                    for(String reward : plugin.getParkourConfig().getStringList(parkour.getName() + ".rewards")) Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), reward.replace("{player}", p.getName()));
                    Cooldown.add(p.getName(), parkour.getName(), 86400, System.currentTimeMillis()); //86400
                    parkouring.remove(p.getName());
                }catch(NullPointerException ex){

                }

            }
            int minimumy = 0;
            for(Location loc : parkour.getLocations()){
                minimumy = parkour.getLocations().get(0).getBlockY();
                if(loc.getBlockY() < minimumy) minimumy = loc.getBlockY();
            }
            if(p.getLocation().getY() < minimumy){
                Location spawn = parkour.getSpawn();
                spawn.setPitch(plugin.getParkourConfig().getInt(parkour.getName() + ".spawn.pitch"));
                spawn.setYaw(plugin.getParkourConfig().getInt(parkour.getName() + ".spawn.yaw"));
                p.teleport(spawn);
                p.sendMessage(ChatColor.RED + "You have fallen!");
                parkouring.remove(p.getName());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent e){
        if(e.getEntity() instanceof Player){
            Player p = (Player) e.getEntity();
            if(parkouring.containsKey(p.getName())){
                e.setCancelled(true);
            }
        }
    }

   @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e){
        if(!parkouring.containsKey(e.getPlayer().getName())) return;
        if(e.getMessage().contains("home") || e.getMessage().contains("spawn") || e.getMessage().contains("warp") || e.getMessage().contains("w ")){
            e.setCancelled(true);
            e.getPlayer().sendMessage(ChatColor.RED + "You cannot use this command in parkour.");
        }
    }

    @EventHandler
         public void onLeave(PlayerQuitEvent e){
        if(parkouring.containsKey(e.getPlayer().getName())){
            Location spawn = parkouring.get(e.getPlayer().getName()).getSpawn();
            parkouring.remove(e.getPlayer().getName());
            e.getPlayer().teleport(spawn);
        }
    }

    @EventHandler
    public void onLeave(PlayerKickEvent e){
        if(parkouring.containsKey(e.getPlayer().getName())){
            Location spawn = parkouring.get(e.getPlayer().getName()).getSpawn();
            parkouring.remove(e.getPlayer().getName());
            e.getPlayer().teleport(spawn);
        }
    }
}
