package pw.soopr.vParkour;

import org.bukkit.entity.Player;

/**
 * Created by Aaron.
 */
public abstract class ParkourCommand {

    public abstract void execute(Player p, String[] args);
}
