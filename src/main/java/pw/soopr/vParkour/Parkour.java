package pw.soopr.vParkour;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron.
 */
public class Parkour {

    String name;
    ArrayList<Location> locations;
    Location spawn;

    public Location getFinish() {
        return finish.clone();
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    Location finish;

    public World getWorld() {
        return world;
    }

    World world;

    public String getName() {
        return name;
    }

    public ArrayList<Location> getBlocks() {
        return locations;
    }

    public Location getSpawn() {
        return spawn.clone();
    }

    public void addBlock(Location loc){
        locations.add(loc);
    }

    public Parkour(String name, ArrayList<Location> locations, Location spawn, World world, Location finish){
        this.name = name;
        this.locations = locations;
        this.spawn = spawn;
        this.world = world;
        this.finish = finish;
    }
    Main plugin;
    public Parkour (Main main){
        this.plugin = main;
    }

    public Location getFirstBlock(){
        return locations.get(0).clone();
    }

    public List<String> getRewards(){
        return plugin.getParkourConfig().getStringList(name + ".rewards");
    }

}
