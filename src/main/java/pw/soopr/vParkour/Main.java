package pw.soopr.vParkour;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import pw.soopr.vParkour.Commands.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Aaron.
 */
public class Main extends JavaPlugin{

    private ArrayList<ParkourCommand> cmds;

    public ArrayList<Parkour> parkours;
    public static HashMap<String, String> editing;
    public static HashMap<UUID, ArrayList<Cooldown>> oncds;

    File file;
    YamlConfiguration parkourconfig;

    public void onEnable(){

       handleCooldowns();
        oncds = new HashMap<UUID, ArrayList<Cooldown>>();
        parkours = new ArrayList<Parkour>();
        editing = new HashMap<String, String>();
        cmds = new ArrayList<ParkourCommand>();
        cmds.add(new Edit(this));
        cmds.add(new Create(this));
        cmds.add(new Remove(this));
        cmds.add(new SetSpawn(this));
        cmds.add(new ListParkours(this));
        cmds.add(new Status(this));
        cmds.add(new SetFinish(this));
        cmds.add(new Register(this));
        cmds.add(new Reload(this));
        cmds.add(new AddReward(this));
        cmds.add(new RemoveReward(this));

        Bukkit.getServer().getPluginManager().registerEvents(new Edit(this), this);
        Bukkit.getServer().getPluginManager().registerEvents(new MoveListener(this), this);

        if(!getDataFolder().exists()) getDataFolder().mkdir();

        file = new File(getDataFolder() + "/parkours.yml");
        if(!file.exists()) try{
            file.createNewFile();
        }catch(IOException e){
            e.printStackTrace();
        }
        parkourconfig = YamlConfiguration.loadConfiguration(file);
        registerParkours();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandlbl, String[] args){
        if(!(sender instanceof Player)){
            sender.sendMessage("Only players can run parkour commands.");
            return true;
        }

        Player p = (Player) sender;
        if(!p.hasPermission("vparkour.admin")){
            p.sendMessage(ChatColor.RED + "No permission.");
            return true;
        }

       if(cmd.getName().equalsIgnoreCase("parkour")){
            if(args.length == 0){
                for(ParkourCommand pcmd : cmds){
                    CommandInfo info = pcmd.getClass().getAnnotation(CommandInfo.class);
                    p.sendMessage(ChatColor.GRAY + "/parkour " + ChatColor.AQUA + info.aliases()[0] + ChatColor.GRAY + ": " + info.desc());
                }
                return true;
            }

            ParkourCommand requested = null;

                outer : for(ParkourCommand pcmd : cmds){
                CommandInfo info = pcmd.getClass().getAnnotation(CommandInfo.class);
                for(String alias : info.aliases()){
                    if(alias.equalsIgnoreCase(args[0])){
                        requested = pcmd;
                        break outer;
                    }
                }
            }

            if(requested == null){
                p.sendMessage(ChatColor.RED + "Invalid args, /parkour for help.");
                return true;
            }

            List<String> newargs = new LinkedList<String>(Arrays.asList(args));
            newargs.remove(0);
            args = newargs.toArray(new String[newargs.size()]);

            requested.execute(p, args);

        }

        return true;
    }

    public ArrayList<Parkour> getParkourList(){
        return parkours;
    }

    public YamlConfiguration getParkourConfig(){
        return parkourconfig;
    }

    public void saveParkourConfig(){
        try{
            parkourconfig.save(file);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void registerParkours(){
        for(String key : getParkourConfig().getKeys(false)){
            ConfigurationSection section = getParkourConfig().getConfigurationSection(key);
            if(section.getBoolean("completed")){
                
                ArrayList<Location> blocks = new ArrayList<Location>();
                for(String str : section.getConfigurationSection("blocks").getKeys(false)){
                    ConfigurationSection block = section.getConfigurationSection("blocks." + str);
                    Location loc = new Location(Bukkit.getWorld(section.getString("world")), block.getDouble("x"), block.getDouble("y"), block.getDouble("z"));
                    blocks.add(loc);
                }      
                Location spawn = new Location(Bukkit.getWorld(section.getString("world")), section.getDouble("spawn.x"), section.getDouble("spawn.y"),section.getDouble("spawn.z"),section.getInt("spawn.pitch"),section.getInt("spawn.yaw"));
                Location finish = new Location(Bukkit.getWorld(section.getString("world")), section.getDouble("finish.x"), section.getDouble("finish.y"),section.getDouble("finish.z"),section.getInt("finish.pitch"),section.getInt("finish.yaw"));
                parkours.add(new Parkour(key, blocks, spawn, Bukkit.getWorld(section.getString("world")), finish));
            }            
        }
    }

    public void handleCooldowns(){
        Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
               Cooldown.handleCooldowns();
            }
        },0,20);
    }


}
