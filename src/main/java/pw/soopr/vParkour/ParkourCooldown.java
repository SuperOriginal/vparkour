package pw.soopr.vParkour;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
public class ParkourCooldown {

    public HashMap<String, ParkourCooldown> cooldownMap = new HashMap<String,ParkourCooldown>();

    public String ability = "";
    public String player = "";
    public long seconds;
    public long systime;

    public ParkourCooldown(String player, long seconds, long systime) {
        this.player = player;
        this.seconds = seconds;
        this.systime = systime;
    }

    public ParkourCooldown(String player) {
        this.player = player;
    }
}
