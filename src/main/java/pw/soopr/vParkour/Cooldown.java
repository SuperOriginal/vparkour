package pw.soopr.vParkour;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Aaron.
 */
public class Cooldown {

    public static HashMap<String,ParkourCooldown> cooldownPlayers = new HashMap<String, ParkourCooldown>();

    public static void add(String player, String ability, long seconds, long systime) {
        if(!cooldownPlayers.containsKey(player)) cooldownPlayers.put(player, new ParkourCooldown(player));
        if(isCooling(player, ability)) return;
        cooldownPlayers.get(player).cooldownMap.put(ability, new ParkourCooldown(player, seconds * 1000, System.currentTimeMillis()));
    }

    public static boolean isCooling(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) return false;
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) return false;
        return true;
    }

    public static double getRemaining(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) return 0.0;
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) return 0.0;
        return utilTime.convert((cooldownPlayers.get(player).cooldownMap.get(ability).seconds + cooldownPlayers.get(player).cooldownMap.get(ability).systime) - System.currentTimeMillis(), utilTime.TimeUnit.SECONDS, 1);
    }

    public static void removeCooldown(String player, String ability) {
        if(!cooldownPlayers.containsKey(player)) {
            return;
        }
        if(!cooldownPlayers.get(player).cooldownMap.containsKey(ability)) {
            return;
        }
        cooldownPlayers.get(player).cooldownMap.remove(ability);
    }

    public static void handleCooldowns() {
        if(cooldownPlayers.isEmpty()) {
            return;
        }
        for(Iterator<String> it = cooldownPlayers.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            for(Iterator<String> iter = cooldownPlayers.get(key).cooldownMap.keySet().iterator(); iter.hasNext();) {
                String name = iter.next();
                if(getRemaining(key, name) <= 0.0) {
                    removeCooldown(key, name);
                }
            }
        }
    }


}
