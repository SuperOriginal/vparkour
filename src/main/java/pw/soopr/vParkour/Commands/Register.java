package pw.soopr.vParkour.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.Parkour;
import pw.soopr.vParkour.ParkourCommand;

import java.util.ArrayList;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Registers a newly made parkour. Should only be run once for each.", aliases = {"register"})
public class Register extends ParkourCommand{

    Main plugin;
    public Register (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour register [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        if(plugin.getParkourConfig().getBoolean(args[0].toLowerCase() + ".completed")){
            p.sendMessage(ChatColor.RED + args[0].toLowerCase() + " is already registered!");
            return;
        }

        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());
        boolean complete = true;
        if(section.getString("world").isEmpty()) complete = false;
        if(section.getConfigurationSection("finish").getKeys(false).isEmpty()) complete = false;
        if(section.getConfigurationSection("spawn").getKeys(false).isEmpty()) complete = false;
        if(section.getConfigurationSection("blocks").getKeys(false).isEmpty()) complete = false;

        if(!complete){
            p.sendMessage(ChatColor.RED + "Failed to register the parkour. One or more components may be missing. /parkour status to find out.");
            return;
        }else{
            registerParkour(args[0].toLowerCase());
            section.set("completed", true);
            p.sendMessage(ChatColor.DARK_AQUA + "Successfully registered " + args[0].toLowerCase() + ". Type /parkour reload to finalize.");
            plugin.saveParkourConfig();
        }

    }

    public void registerParkour(String key){
            ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(key);
            if(section.getBoolean("completed")){

                ArrayList<Location> blocks = new ArrayList<Location>();
                for(String str : section.getConfigurationSection("blocks").getKeys(false)){
                    ConfigurationSection block = section.getConfigurationSection("blocks." + str);
                    Location loc = new Location(Bukkit.getWorld(section.getString("world")), block.getDouble("x"), block.getDouble("y"), block.getDouble("z"));
                    blocks.add(loc);
                }
                Location spawn = new Location(Bukkit.getWorld(section.getString("world")), section.getDouble("spawn.x"), section.getDouble("spawn.y"),section.getDouble("spawn.z"),section.getInt("spawn.pitch"),section.getInt("spawn.yaw"));
                Location finish = new Location(Bukkit.getWorld(section.getString("world")), section.getDouble("finish.x"), section.getDouble("finish.y"),section.getDouble("finish.z"),section.getInt("finish.pitch"),section.getInt("finish.yaw"));
                plugin.parkours.add(new Parkour(key, blocks, spawn, Bukkit.getWorld(section.getString("world")), finish));

        }
    }
}
