package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Removes a parkour.", aliases = {"remove", "r"})
public class Remove extends ParkourCommand{
    Main plugin;
    public Remove (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour remove [name]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        plugin.getParkourConfig().set(args[0].toLowerCase(), null);
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.GRAY + "Successfully removed " + ChatColor.AQUA + args[0]);

    }
}
