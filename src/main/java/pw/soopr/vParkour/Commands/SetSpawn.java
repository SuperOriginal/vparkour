package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Set the beginning location for a parkour.", aliases = {"setspawn", "ss"})
public class SetSpawn extends ParkourCommand {

    Main plugin;
    public SetSpawn (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour setspawn [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());

        section.set("spawn.x", p.getLocation().getX());
        section.set("spawn.y", p.getLocation().getY());
        section.set("spawn.z", p.getLocation().getZ());
        section.set("spawn.pitch", p.getLocation().getPitch());
        section.set("spawn.yaw", p.getLocation().getYaw());

        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.GRAY + "Set spawn for " + ChatColor.AQUA + args[0]);

       }
}
