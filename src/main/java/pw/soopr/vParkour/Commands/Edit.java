package pw.soopr.vParkour.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

import java.util.HashMap;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Edits a parkour's block composition. Right click to add, left click to remove.", aliases = {"edit", "e"})
public class Edit extends ParkourCommand implements Listener{



    Main plugin;
    public Edit (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {

        if(Main.editing.containsKey(p.getName())){
            Main.editing.remove(p.getName());
            p.sendMessage(ChatColor.DARK_AQUA + "No longer in editing mode.");
            return;
        }

        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour edit [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        Main.editing.put(p.getName(), args[0].toLowerCase());
        p.sendMessage(ChatColor.GRAY + "You are now editing " + ChatColor.DARK_AQUA + args[0].toLowerCase() + ChatColor.GRAY + ". Type /parkour edit to exit.");
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if(e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
            if(Main.editing.containsKey(e.getPlayer().getName())){
                e.setCancelled(true);
                ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(Main.editing.get(e.getPlayer().getName()) + ".blocks");
                if(section == null){
                    e.getPlayer().sendMessage(ChatColor.RED + "That block does not exist in " + Main.editing.get(e.getPlayer().getName()));
                    return;
                }
                boolean found = false;
                for(String key : section.getKeys(false)){
                    ConfigurationSection block = plugin.getParkourConfig().getConfigurationSection(Main.editing.get(e.getPlayer().getName().toLowerCase()) + ".blocks." + key);

                    if(e.getClickedBlock().getX() == block.getInt("x") && e.getClickedBlock().getY() == block.getInt("y") && e.getClickedBlock().getZ() == block.getInt("z")){
                        section.set(key, null);
                        plugin.saveParkourConfig();
                        e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "Removed block from " + Main.editing.get(e.getPlayer().getName()));
                        found = true;
                        break;
                    }
                }
                if(!found){
                    e.getPlayer().sendMessage(ChatColor.RED + "That block does not exist in " + Main.editing.get(e.getPlayer().getName()));
                }
            }
        }

        if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            if(Main.editing.containsKey(e.getPlayer().getName())){
                e.setCancelled(true);
                ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(Main.editing.get(e.getPlayer().getName()).toLowerCase() + ".blocks");
                for(String key : section.getKeys(false)){
                    ConfigurationSection block = plugin.getParkourConfig().getConfigurationSection(Main.editing.get(e.getPlayer().getName()) + ".blocks." + key);
                    if(e.getClickedBlock().getX() == block.getInt("x") && e.getClickedBlock().getY() == block.getInt("y") && e.getClickedBlock().getZ() == block.getInt("z")){
                        e.getPlayer().sendMessage(ChatColor.RED + "That block already is added in " + Main.editing.get(e.getPlayer().getName()));
                        return;
                    }

                }
                String size = String.valueOf(section.getKeys(false).size() + 1);
                section.set(String.valueOf(size) + ".x", e.getClickedBlock().getX());
                section.set(String.valueOf(size) + ".y", e.getClickedBlock().getY());
                section.set(String.valueOf(size) + ".z", e.getClickedBlock().getZ());
                plugin.saveParkourConfig();
                e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "Successfully added block to " + Main.editing.get(e.getPlayer().getName()));
            }
        }
    }
}
