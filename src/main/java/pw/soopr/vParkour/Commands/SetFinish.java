package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Set the finish point of a parkour.", aliases = {"setfinish", "sf"})
public class SetFinish extends ParkourCommand{

    Main plugin;
    public SetFinish (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour setfinish [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        ConfigurationSection finish = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase() + ".finish");
        Block b = p.getLocation().subtract(0,1,0).getBlock();
        finish.set("x", b.getX());
        finish.set("y", b.getY());
        finish.set("z", b.getZ());
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.AQUA + "Set finish point for " + args[0].toLowerCase());


    }
}
