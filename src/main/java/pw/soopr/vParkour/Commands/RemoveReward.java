package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

import java.util.List;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Remove a reward from a parkour.", aliases = {"removereward", "rr"})
public class RemoveReward extends ParkourCommand{

    Main plugin;
    public RemoveReward(Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour removereward [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        StringBuilder builder = new StringBuilder();
        for(int i = 1; i < args.length ; i++){
            builder.append(args[i]).append(" ");
        }
        String reward = builder.toString();

        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());
        if(!section.getStringList("rewards").contains(reward)){
            p.sendMessage(ChatColor.RED + "/" + reward + " is not associated with " + args[0].toLowerCase());
            return;
        }
        List<String> rewards = section.getStringList("rewards");
        rewards.remove(reward);
        section.set("rewards", rewards);
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.AQUA + "/" + reward + " has been removed from " + args[0].toLowerCase());
    }
}
