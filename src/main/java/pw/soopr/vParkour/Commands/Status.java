package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Displays the status of a parkour.", aliases = {"status", "s"})
public class Status extends ParkourCommand{

    Main plugin;
    public Status(Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct usage: /parkour status [name]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());

        //Handling world:
        String world;
        try{
           world = ChatColor.AQUA + section.getString("world");
        }catch(NullPointerException e){
            world = "null"; //Should not happen.
        }

        //Handling Spawn:
        String spawn;
        try{
            spawn = ChatColor.GRAY + "X: " + ChatColor.AQUA + Math.floor(section.getInt("spawn.x")) + ChatColor.GRAY + " Y: " + ChatColor.AQUA + Math.floor(section.getInt("spawn.y")) + ChatColor.GRAY + " Z: " + ChatColor.AQUA + Math.floor(section.getInt("spawn.z"));
        }catch (NullPointerException e){
            spawn = ChatColor.RED + "Not set.";
        }

        //Handling Finish:
        String finish;
        try{
            finish = ChatColor.GRAY + "X: " + ChatColor.AQUA + Math.floor(section.getInt("finish.x")) + ChatColor.GRAY + " Y: " + ChatColor.AQUA + Math.floor(section.getInt("finish.y")) + ChatColor.GRAY + " Z: " + ChatColor.AQUA + Math.floor(section.getInt("finish.z"));
        }catch (NullPointerException e){
            finish = ChatColor.RED + "Not set.";
        }

        //Handling Blocks:
        int blocksset;
        try{
            blocksset = section.getConfigurationSection("blocks").getKeys(false).size();
        }catch (NullPointerException e){
            blocksset = 0;
        }

        //Handling Registered:
        boolean registered = false;
        if(section.getBoolean("completed")) registered = true;

        p.sendMessage(ChatColor.GRAY + "Status of: " + ChatColor.AQUA + args[0].toLowerCase());
        p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "World: " + world);
        p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "Spawn: " + spawn);
        p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "Finish: " + finish);
        p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "Blocks set: " + ChatColor.AQUA + blocksset);
        p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "Registered: " + ChatColor.AQUA + registered);
    }
}
