package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Reloads parkour config.", aliases = {"reload"})
public class Reload extends ParkourCommand{

    Main plugin;
    public Reload (Main main){
        this.plugin = main;
    }
    @Override
    public void execute(Player p, String[] args) {
        plugin.reloadConfig();
        plugin.getParkourList().clear();
        plugin.registerParkours();
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.AQUA + "Reloaded config.");
    }
}
