package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Creates a parkour.", aliases = {"create", "c"})
public class Create extends ParkourCommand{

    public Main plugin;

    public Create (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour create [name]");
        }

        for(String key : plugin.getParkourConfig().getKeys(false)){
            if(key.equalsIgnoreCase(args[0])){
                p.sendMessage(ChatColor.RED + "A parkour with that name already exists!");
                return;
            }
        }
        plugin.getParkourConfig().createSection(args[0].toLowerCase());
        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());
        plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase()).set("world", p.getWorld().getName());
        section.createSection("blocks");
        section.createSection("spawn");
        section.createSection("finish");
        section.createSection("rewards");
        plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase()).set("completed", false);
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.GRAY + "Successfully created parkour " + ChatColor.AQUA +  args[0]);

    }
}
