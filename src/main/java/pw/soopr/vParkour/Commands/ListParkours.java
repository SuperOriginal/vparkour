package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

import java.util.ArrayList;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "list all complete/incomplete parkours.", aliases = {"list", "l"})
public class ListParkours extends ParkourCommand {

    Main plugin;
    public ListParkours (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        ArrayList<String> incomplete = new ArrayList<String>();
        ArrayList<String> complete = new ArrayList<String>();
        for(String key : plugin.getParkourConfig().getKeys(false)){
            if(plugin.getParkourConfig().getBoolean(key + ".completed")) complete.add(key);
            else incomplete.add(key);
        }

        p.sendMessage(ChatColor.GRAY + "Complete parkours:");
        for(String string : complete)  p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.DARK_AQUA + string);
        p.sendMessage(ChatColor.GRAY + "Incomplete parkours:");
        for(String string : incomplete)  p.sendMessage(ChatColor.DARK_GRAY + "- " + ChatColor.RED + string);
    }
}
