package pw.soopr.vParkour.Commands;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import pw.soopr.vParkour.CommandInfo;
import pw.soopr.vParkour.Main;
import pw.soopr.vParkour.ParkourCommand;

import java.util.List;

/**
 * Created by Aaron.
 */
@CommandInfo(desc = "Add a reward to a parkour.", aliases = {"addreward", "ar"})
public class AddReward extends ParkourCommand{

    Main plugin;
    public AddReward (Main main){
        this.plugin = main;
    }

    @Override
    public void execute(Player p, String[] args) {
        if(args.length == 0){
            p.sendMessage(ChatColor.RED + "Correct Usage: /parkour addreward [parkourname]");
            return;
        }

        if(!plugin.getParkourConfig().getKeys(false).contains(args[0].toLowerCase())){
            p.sendMessage(ChatColor.RED + "A parkour with that name does not exist!");
            return;
        }

        StringBuilder builder = new StringBuilder();
        for(int i = 1; i < args.length ; i++){
            builder.append(args[i]).append(" ");
        }
        String reward = builder.toString();

        ConfigurationSection section = plugin.getParkourConfig().getConfigurationSection(args[0].toLowerCase());
        List<String> rewards = section.getStringList("rewards");
        rewards.add(reward);
        section.set("rewards", rewards);
        plugin.saveParkourConfig();
        p.sendMessage(ChatColor.AQUA + "/" + reward + " will be run when " + args[0].toLowerCase() + " is completed.");
    }
}
