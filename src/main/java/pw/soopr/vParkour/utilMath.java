package pw.soopr.vParkour;

import java.text.DecimalFormat;

/**
 * Created by Aaron.
 */
public class utilMath {

    public static double trim(double untrimmeded, int decimal) {
        String format = "#.#";

        for(int i = 1; i < decimal; i++) {
            format = format + "#";
        }
        DecimalFormat twoDec = new DecimalFormat(format);
        return Double.valueOf(twoDec.format(untrimmeded)).doubleValue();
    }
}
